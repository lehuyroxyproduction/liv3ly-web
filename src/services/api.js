import { Params } from '../contants/contants';

export const verifyCode = async code => {
  const res = await postData(
    Params.API_URL + '/account/verify-reset-password-code',
    code
  )
  return res
}

export const resetPassword = async params => {
  const res = await postData(
    Params.API_URL + '/account/reset-password',
    params
  )
  return res
}
export const login = async params => {
  const res = await postData(
    Params.API_URL + '/account/login',
    params
  )
  return res
}
export const verifyAccount = async params => {
  const res = await postData(
    Params.API_URL + '/account/verify',
    params
  )
  return res
}

const postData = async (url, params) => {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, cors, *same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrer: 'no-referrer', // no-referrer, *client
    body: JSON.stringify(params) // body data type must match "Content-Type" header
  })

  return response
}

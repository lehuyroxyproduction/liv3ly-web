import React from 'react'
import { Upload, Icon, message } from 'antd'
import { UploadStyle } from './upload.style'

class ImageUpload extends React.Component {
  onChange = () => {
    console.log('change')
  }
  render() {
    const uploadButton = (
      <div>
        <Icon type={this.props.loading ? 'loading' : 'plus'} />
        <div className='ant-upload-text'>Upload</div>
      </div>
    )
    const { imageUrl } = this.props
    return (
      <UploadStyle>
        <Upload
          name='photo'
          accept='image'
          listType='picture-card'
          // className='avatar-uploader'
          showUploadList={false}
          beforeUpload={this.props.beforeUpload}
          onChange={this.onChange}
        >
          {imageUrl ? (
            <img style={{ maxWidth: '100%' }} src={imageUrl} alt='avatar' />
          ) : (
            uploadButton
          )}
        </Upload>
      </UploadStyle>
    )
  }
}

export default ImageUpload

import React from 'react'
import { Icons } from '../../contants/contants'

class Loyalty extends React.Component {
  state = {
    country: 183
  }

  componentDidMount() {
    let urlParams = this.getUrlParams(window.location.href)
    console.log(urlParams)

    if (urlParams.hasOwnProperty('base')) {
      this.setState({ country: Number(urlParams.base) })
    }
    console.log(urlParams.base)
  }

  getUrlParams = urlOrQueryString => {
    if (urlOrQueryString.indexOf('?') >= 0) {
      const i = urlOrQueryString.indexOf('?')
      const queryString = urlOrQueryString.substring(i + 1)
      if (queryString) {
        return this._mapUrlParams(queryString)
      }
    }

    return {}
  }

  _mapUrlParams = queryString => {
    return queryString
      .split('&')
      .map(function(keyValueString) {
        console.log(keyValueString)
        return keyValueString.split('=')
      })
      .reduce(function(urlParams, [key, value]) {
        if (Number.isInteger(parseInt(value)) && parseInt(value) === value) {
          urlParams[key] = parseInt(value)
        } else {
          urlParams[key] = decodeURI(value)
        }
        return urlParams
      }, {})
  }

  render() {
    let exRate = ''
    switch (this.state.country) {
      case 222:
        exRate = '17,000 VND'
        break
      default:
        exRate = '1 SGD'
        break
    }

    return (
      <div className='site_wrapper' id='site_wrapper'>
        {/* <div className='loyalty d-flex flex-column align-items-center justify-align-center pt-5 pb-4'>
          <img className='mb-4' src='images/imgRewards.png' alt='' />
          <h2 style={{ fontWeight: 900 }}>Redeemable Points</h2>
          <div className='points'>
            <div className='d-flex align-items-center mb-2'>
              <img
                className='mr-2'
                width='24'
                src='images/icPoint.png'
                alt=''
              />
              <div className='point' style={{ fontWeight: 900 }}>
                740 points
              </div>
            </div>
            <div
              className='d-flex align-items-center justify-content-center'
              style={{ color: '#D0D0D0', fontWeight: 900 }}
            >
              Point History{' '}
              <img className='ml-2' src='images/icArrowRight.png' alt='' />
            </div>
          </div>
        </div> */}
        {/* <div className='reward-line mb-4' /> */}
        <div className='how-to-earn col-12 pt-4 pb-4 '>
          <h6>How do I earn Liv3ly points</h6>
          <div className='body-how-to-earn mt-3'>
            <div className='d-flex align-items-start justify-content-start mb-3'>
              <div className='mr-4'>
                <img src={Icons.icGift} width='20' height='20' alt='' />
              </div>
              <div>
                <span style={{ fontSize: '18px' }}>
                  Perform daily using our app
                </span>{' '}
                <br />
                <i>
                  <span>
                    Earn <strong>1 point</strong> for every kilometre you run
                  </span>
                </i>
              </div>
            </div>
            <div className='d-flex align-items-start justify-content-start mb-3'>
              <div className='mr-4'>
                <img src={Icons.icGift} width='20' height='20' alt='' />
              </div>
              <div>
                <span style={{ fontSize: '18px' }}>
                  Register for our virtual races or events
                </span>{' '}
                <br />
                <i>
                  <span>
                    Earn <strong>2 points</strong> for every{' '}
                    <span
                      className='ex-rate'
                      style={{ color: 'blue', fontWeight: 'bold' }}
                    >
                      {exRate}
                    </span>{' '}
                    you spend
                  </span>
                </i>
              </div>
            </div>
            <div className='d-flex align-items-start justify-content-start mb-3'>
              <div className='mr-4'>
                <img src={Icons.icGift} width='20' height='20' alt='' />
              </div>
              <div>
                <span style={{ fontSize: '18px' }}>Increase your points</span>{' '}
                <br />
                <i>
                  <span>
                    Earn <strong>1.5 points</strong> for every{' '}
                    <span
                      className='ex-rate'
                      style={{ color: 'blue', fontWeight: 'bold' }}
                    >
                      {exRate}
                    </span>{' '}
                    you top up
                  </span>
                </i>
              </div>
            </div>
            <div className='d-flex align-items-start justify-content-start'>
              <div className='mr-4'>
                <img src={Icons.icGift} width='20' height='20' alt='' />
              </div>
              <div>
                <span style={{ fontSize: '18px' }}>Complete challenges</span>
              </div>
            </div>
          </div>
        </div>
        <div className='reward-line mb-4' />
        <div className='how-to-use col-12 mb-4'>
          <h6>How do I earn Liv3ly points</h6>
          <div className='d-flex'>
            <div className='mr-4'>
              <img src={Icons.icCheck} alt='' />
            </div>
            <div>Run or walk with tracker</div>
          </div>
          <div className='d-flex'>
            <div className='mr-4'>
              <img src={Icons.icCheck} alt='' />
            </div>
            <div>Register for races, events and challenges</div>
          </div>
          <div className='d-flex'>
            <div className='mr-4'>
              <img src={Icons.icCheck} alt='' />
            </div>
            <div>Purchase points</div>
          </div>
        </div>
      </div>
    )
  }
}

export default Loyalty

import React from 'react'
import { Icons, Params ,wp ,hp} from '../../contants/contants';

import * as api from '../../services/api';
import { Spin, Icon } from 'antd';


class VerifyAccount extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      done: false,
      notifyText: '',
      isError: false,
      isLoading: true
    }
  }

  componentDidMount() {
    setTimeout(this.checkVerify, 3000)
  }

  checkVerify = async () => {
    let urlParams = this.getUrlParams(window.location.href)
    if (urlParams.hasOwnProperty('code')) {
      const payload = {
        "code": `${urlParams.code}`,
        "id": parseInt(urlParams.id),
        "type": parseInt(urlParams.type)
      }
      const url = Params.API_URL + '/account/verify'
      console.log(url)
      console.log(payload)

      const data = await fetch(url, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(payload)
      })
        .then(response => response.json())
        .catch(error => error)

      if (data.status_code == 1000) {
        this.setState({
          done: !this.state.done,
          isLoading: !this.state.isLoading
        })
      } else {
        this.setState({
          isError: !this.state.isError,
          isLoading: !this.state.isLoading
        })
      }
    }
  }

  getUrlParams = urlOrQueryString => {
    if (urlOrQueryString.indexOf('?') >= 0) {
      const i = urlOrQueryString.indexOf('?')
      const queryString = urlOrQueryString.substring(i + 1)
      if (queryString) {
        return this._mapUrlParams(queryString)
      }
    }
    return {}
  }

  _mapUrlParams = queryString => {
    return queryString
      .split('&')
      .map(function (keyValueString) {
        return keyValueString.split('=')
      })
      .reduce(function (urlParams, [key, value]) {
        if (Number.isInteger(parseInt(value)) && parseInt(value) === value) {
          urlParams[key] = parseInt(value)
        } else {
          urlParams[key] = decodeURI(value)
        }
        return urlParams
      }, {})
  }


  render() {
    const { done, notifyText, isError } = this.state

    const container = <div>
      <div>
        <h2 style={{ textAlign: "center" }}>
          {
            isError && !done ? "Sorry, The code is invalid or this account has been activated!" :
              !isError && done ?
                "Congrats, your account has been activated successfully!" :
                "Verifying your account..."
          }
        </h2>
      </div>
      <div className=' d-flex flex-column justify-content-center mx-auto align-items-center '>
        {
          isError && !done ?
            <img src={Icons.icError} height={wp > hp ? wp * 0.15 : hp * 0.15} /> :
            !isError && done ?
              <img src={Icons.icDone} height={wp > hp ? wp * 0.15 : hp * 0.15} /> :
              <img src={Icons.icNotDone} height={wp > hp ? wp * 0.15 : hp * 0.15} />
        }
      </div>
    </div>

    return (
      <div class="d-flex align-items-center justify-content-center" style={{ height: "100vh", width: "100%", padding: "10px" }}>
        <Spin
          spinning={isError && !done ? false : !isError && done ? false : true}
          delay={500}
        >
          {container}
        </Spin>
      </div>
    )
  }
}

export default VerifyAccount

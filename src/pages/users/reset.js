import React from 'react'
import { Form, Button } from 'react-bootstrap'
import * as api from '../../services/api'

class ResetPassword extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      password: '',
      passwordConfirm: '',
      feedback: '',
      validate: false,
      verifyCode: false,
      code: '',
      notifyText: '',
      accountId:''
    }
  }

  async componentDidMount() {
    let urlParams = this.getUrlParams(window.location.href)
    console.log(urlParams)
    if (urlParams.hasOwnProperty('code')) {
      const data = await api.verifyCode({ code: urlParams.code , accountId : parseInt(urlParams.id)})
      if (data && data.status === 200) {
        this.setState({
          verifyCode: true,
          code: urlParams.code,
          accountId: parseInt(urlParams.id)
        })
      } else {
        this.setState({
          notifyText:
            'This password reset code was not found. Please inform admin!'
        })
        console.log('error', data)
      }
    }
  }

  getUrlParams = urlOrQueryString => {
    if (urlOrQueryString.indexOf('?') >= 0) {
      const i = urlOrQueryString.indexOf('?')
      const queryString = urlOrQueryString.substring(i + 1)
      if (queryString) {
        return this._mapUrlParams(queryString)
      }
    }

    return {}
  }

  _mapUrlParams = queryString => {
    return queryString
      .split('&')
      .map(function(keyValueString) {
        return keyValueString.split('=')
      })  
      .reduce(function(urlParams, [key, value]) {
        if (Number.isInteger(parseInt(value)) && parseInt(value) === value) {
          urlParams[key] = parseInt(value)
        } else {
          urlParams[key] = decodeURI(value)
        }
        return urlParams
      }, {})
  }

  handleSubmit = async e => {
    const { password,  validate, accountId , code } = this.state
    if (!validate) {
      e.preventDefault()
      e.stopPropagation()
    } else {
      e.preventDefault()
      e.stopPropagation()
      const data = await api.resetPassword({ code: code , accountId : accountId , password: password })
      if (data && data.status === 200) {
        this.setState({
          verifyCode: false,
          notifyText: 'Update password successfully!'
        })
      } else {
        console.log('fail')
      }
    }
  }

  render() {
    const {
      password,
      passwordConfirm,
      feedback,
      verifyCode,
      notifyText
    } = this.state

    return verifyCode ? (
      <div
        style={{ height: '100vh' }}
        className='site_wrapper d-flex align-items-center justify-content-center'
        id='site_wrapper'
      >
        <div className='pl-3 pr-3'>
          <div style={{ paddingBottom: '60px' }}>
            <b style={{ fontSize: '30px' }}>Reset Password</b>
            <br />
          </div>
          <Form onSubmit={e => this.handleSubmit(e)}>
            <Form.Group controlId='formGroupEmail'>
              <Form.Label>
                New Password (at least 8 characters containing 1 alphabet and 1
                number)*
              </Form.Label>
              <Form.Control
                type='password'
                placeholder='New password'
                value={password}
                isValid={
                  password.length >= 8 &&
                  /\d/.test(password) &&
                  /[a-zA-Z]/.test(password)
                }
                onBlur={() => {
                  if (
                    password.length < 8 ||
                    !/\d/.test(password) ||
                    !/[a-zA-Z]/.test(password)
                  ) {
                    this.setState({
                      validate: false,
                      feedback:
                        'New password must be at least 8 characters and contain 1 alphabet and 1 number!'
                    })
                  } else if (passwordConfirm && password !== passwordConfirm) {
                    this.setState({
                      feedback: 'Two passwords that you enter is inconsistent!',
                      validate: false
                    })
                  } else {
                    this.setState({ validate: true, feedback: '' })
                  }
                }}
                onChange={e => {
                  this.setState(
                    {
                      password: e.currentTarget.value,
                      feedback: ''
                    },
                    () => {
                      console.log(password)
                    }
                  )
                }}
              />
            </Form.Group>
            <Form.Group controlId='formGroupPassword'>
              <Form.Label>Re-type Your Password*</Form.Label>
              <Form.Control
                type='password'
                placeholder='Confirm new password'
                value={passwordConfirm}
                isValid={passwordConfirm !== '' && password === passwordConfirm}
                onChange={e => {
                  this.setState({
                    passwordConfirm: e.currentTarget.value
                  })
                }}
                onBlur={() => {
                  if (!passwordConfirm) {
                    this.setState({
                      validate: false,
                      feedback: 'Please confirm your new password'
                    })
                  } else if (password !== passwordConfirm) {
                    this.setState({
                      feedback: 'Two passwords that you enter is inconsistent!',
                      validate: false
                    })
                  } else if (
                    password.length < 8 ||
                    !/\d/.test(password) ||
                    !/[a-zA-Z]/.test(password)
                  ) {
                    this.setState({
                      validate: false,
                      feedback:
                        'New password must be at least 8 characters and contain 1 alphabet and 1 number!'
                    })
                  } else {
                    this.setState({ feedback: '', validate: true })
                  }
                }}
              />

              <p
                style={{
                  fontSize: '80%',
                  color: '#dc3545',
                  lineHeight: '1.5',
                  marginTop: '8px'
                }}
              >
                {feedback}
              </p>
            </Form.Group>
            <Button type='submit' variant='primary' size='lg'>
              Update
            </Button>
          </Form>
        </div>
      </div>
    ) : (
      <div className='p-3'>{notifyText}</div>
    )
  }
}

export default ResetPassword

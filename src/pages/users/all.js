import React from 'react'
import { Backgrounds } from '../../contants/contants'

class AllRewards extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      list: [
        // {
        //   title: 'SGD5 Off Your Next Race Registration',
        //   banner: Backgrounds.Bg1,
        //   point: 300,
        //   value: 20
        // },
        // {
        //   title: 'SGD10 Off Your Next Race Registration',
        //   banner: Backgrounds.Bg2,
        //   point: 500,
        //   value: 40
        // },
        // {
        //   title: 'Race day slots promo code',
        //   banner: Backgrounds.Bg3,
        //   point: 3000,
        //   value: 20
        // }
      ]
    }
  }

  renderlist = item => {
    const { title, banner, point, value } = item
    return (
      <div className='reward col-12 mb-4 mt-4'>
        <img
          src={banner}
          className='image-box d-flex justify-content-center'
          style={{ marginBottom: '11px' }}
          width='100%'
        />

        <div className='reward-body'>
          <h6 className='card-title'>{title}</h6>
          <div className='d-flex justify-content-between'>
            <div>
              <img
                style={{ marginTop: '-2px' }}
                className='mr-2'
                src='../images/icPoint.png'
                width='20'
                alt=''
              />
              <span className='point'>{point} points</span>
            </div>
            <div className='d-flex align-items-center justify-content-between p-0'>
              <div
                className='d-flex'
                style={{ height: '8px', marginRight: '30px' }}
              >
                <div
                  className='col-3'
                  style={{
                    backgroundColor: '#004AFF',
                    borderRadius: '4px 4px'
                  }}
                />
                <div
                  className='col-9'
                  style={{
                    backgroundColor: '#F4F4F4',
                    borderTopRightRadius: '4px 4px',
                    borderBottomRightRadius: '4px 4px'
                  }}
                />
              </div>
              <div className='slot'>{value}/100 slots</div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    const lenght =this.state.list.length;
    return (
      <div className='site_wrapper' id='site_wrapper'>
        <h6 className='pl-3 pr-3 mt-3 mb-0' style={{ fontWeight: 900 }}>
          Race Day Privileges ({lenght})
        </h6>

        {
        //   this.state.list.map(item => {
        //   return this.renderlist(item)
        // })
      }
      <div
      style={{ height: '100px' }}
      className='my-reward d-flex justify-content-center mx-auto align-items-center shadow-sm rounded mt-3 '
    >
      Coming soon...
    </div>
        {/* <div className='reward col-12 mb-4 mt-4'>
          <img
            src={Backgrounds.Bg1}
            className='image-box d-flex justify-content-center'
            style={{ marginBottom: '11px' }}
            width='100%'
          />

          <div className='reward-body'>
            <h6 className='card-title'>SGD5 Off Your Next Race Registration</h6>
            <div className='d-flex justify-content-between'>
              <div>
                <img
                  style={{ marginTop: '-2px' }}
                  className='mr-2'
                  src='images/icPoint.png'
                  width='20'
                  alt=''
                />
                <span className='point'>300 points</span>
              </div>
              <div className='d-flex align-items-center justify-content-between p-0'>
                <div
                  className='d-flex'
                  style={{ height: '8px', marginRight: '30px' }}
                >
                  <div
                    className='col-3'
                    style={{
                      backgroundColor: '#004AFF',
                      borderRadius: '4px 4px'
                    }}
                  />
                  <div
                    className='col-9'
                    style={{
                      backgroundColor: '#F4F4F4',
                      borderTopRightRadius: '4px 4px',
                      borderBottomRightRadius: '4px 4px'
                    }}
                  />
                </div>
                <div className='slot'>19/100 slots</div>
              </div>
            </div>
          </div>
        </div> */}

        {/* <div className='reward col-12 mb-4'>
          <img
            src={Backgrounds.Bg2}
            className='image-box d-flex justify-content-center'
            style={{ marginBottom: '11px' }}
            width='100%'
          />

          <div className='reward-body'>
            <h6 className='card-title'>
              SGD10 Off Your Next Race Registration
            </h6>
            <div className='d-flex justify-content-between'>
              <div>
                <img
                  style={{ marginTop: '-2px' }}
                  className='mr-2'
                  src='images/icPoint.png'
                  width='20'
                  alt=''
                />
                <span className='point'>550 points</span>
              </div>
              <div className='d-flex align-items-center justify-content-between p-0'>
                <div
                  className='d-flex'
                  style={{ height: '8px', marginRight: '30px' }}
                >
                  <div
                    className='col-3'
                    style={{
                      backgroundColor: '#004AFF',
                      borderRadius: '4px 4px'
                    }}
                  />
                  <div
                    className='col-9'
                    style={{
                      backgroundColor: '#F4F4F4',
                      borderTopRightRadius: '4px 4px',
                      borderBottomRightRadius: '4px 4px'
                    }}
                  />
                </div>
                <div className='slot'>19/100 slots</div>
              </div>
            </div>
          </div>
        </div> */}

        {/* <div className='reward col-12 mb-4'>
          <img
            src={Backgrounds.Bg3}
            className='image-box d-flex justify-content-center'
            style={{ marginBottom: '11px' }}
            width='100%'
          />

          <div className='reward-body'>
            <h6 className='card-title'>Race day slots promo code</h6>
            <div className='d-flex justify-content-between'>
              <div>
                <img
                  style={{ marginTop: '-2px' }}
                  className='mr-2'
                  src='images/icPoint.png'
                  width='20'
                  alt=''
                />
                <span className='point'>3,000 points</span>
              </div>
              <div className='d-flex align-items-center justify-content-between p-0'>
                <div
                  className='d-flex'
                  style={{ height: '8px', marginRight: '30px' }}
                >
                  <div
                    className='col-3'
                    style={{
                      backgroundColor: '#004AFF',
                      borderRadius: '4px 4px'
                    }}
                  />
                  <div
                    className='col-9'
                    style={{
                      backgroundColor: '#F4F4F4',
                      borderTopRightRadius: '4px 4px',
                      borderBottomRightRadius: '4px 4px'
                    }}
                  />
                </div>
                <div className='slot'>19/50 slots</div>
              </div>
            </div>
          </div>
        </div> */}
      </div>
    )
  }
}

export default AllRewards

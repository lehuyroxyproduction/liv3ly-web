import React from 'react'
import {
  wp,
  hp,
  Params,
  Icons,
  METHOD_GET,
  METHOD_DEL,
  DATE_FORMAT
} from '../../../contants/contants'
import { Button, Spin, Checkbox, DatePicker } from 'antd'
import FormChallenge from './form'

import { Link, Redirect } from 'react-router-dom'
import { Container, Component, ScrollView } from './style/edit.style'
import moment from 'moment'
import 'antd/dist/antd.css'
const { RangePicker } = DatePicker

class CreateChallenge extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      redirectToReferrer: false
    }
  }

  render() {
    const { redirectToReferrer } = this.state
    let { from } = this.props.location.state || {
      from: { pathname: '/admin/challenges' }
    }

    if (redirectToReferrer) return <Redirect to={from} />

    return (
      <Container
        className='site_wrapper d-flex flex-column align-items-center justify-content-center'
        id='site_wrapper'
      >
        <Component>
          <FormChallenge type='create' />
        </Component>
      </Container>
    )
  }
}

export default CreateChallenge

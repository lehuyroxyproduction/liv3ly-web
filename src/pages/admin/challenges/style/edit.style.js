import styled from 'styled-components'
import { Backgrounds, wp, hp, Colors } from '../../../../contants/contants'

export const Container = styled.div`
  background-color: #004dd9;
  padding: 48px 0;
`
export const Component = styled.div`
  width: 100%;
  max-width: 720px;
  border-radius: 8px;
  background-color: #ffffff;
  padding: 16px;
  input,
  textarea {
    border-radius: 4px;
  }
  .ant-btn {
    right: 0;
    top: 0;
    border: 1px solid black;
    background-color: white;
    width: 30px;
    height: 30px;
    padding: 0;
    position: absolute;
    border-radius: 50%;
  }
  .ant-btn:hover {
    border-radius: 50%;
    border: 2px solid ${Colors.blueTable};
    // background-color: ;
    color: ${Colors.blueTable};
  }
  .ant-input {
    width: ${wp * 0.3 - 40 + 'px'};
  }
  .ant-checkbox-wrapper {
    margin-bottom: 15px;
    // height:15px;
  }
  .ant-calendar-picker {
    margin-bottom: 15px;
  }
  img {
    width: ${wp * 0.3 - 40 + 'px'};
    margin: 10px 0 20px 0;
  }
  button {
    width: ${wp * 0.3 - 40 + 'px'};
    height: 30px;
    border-radius: 4px;
  }
`
export const ScrollView = styled.div`
  height: ${hp * 0.9 - 40 + 'px'};
  padding: 42px 0;
  overflow-x: scroll;
  position: relative;
`
export const Contents = styled.div`
  width: ${wp * 0.3 - 30 + 'px'}!important;
`

export const InputStyle = styled.input`
    margin-bottom:15px;
    width:${wp * 0.3 - 40 + 'px'}
    border:0;
    padding:5px;
`

export const TitleStyle = styled.h6`
  margin-bottom: 15px;
`
export const InputDesStyle = styled.textarea`
    width:${wp * 0.3 - 40 + 'px'}
    border:0;
    resize:none;
    padding:5px;
    height:${hp * 0.1 + 'px'};
    margin-bottom:15px;
`

export const BtnStyle = styled.button``

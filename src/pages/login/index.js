import React from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Form, Icon, Input, Button, Spin, Modal } from 'antd'
import {
  Icons,
  Params,
  wp,
  hp,
  Regex,
  Backgrounds
} from '../../contants/contants'
import { LoginStyle, FromLoginStyle, TitleStyle } from './login.styles'
import 'antd/dist/antd.css'
import * as api from '../../services/api'
// import { connect } from 'react-redux'
// import {postAPILoginStart} from '../Redux/Action/actionLogin'
// import { bindActionCreators } from 'redux';

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      passError: false,
      redirectToReferrer: false,
      layout: 'inline',
      progress: false,
      notitext: '',
      isError: false
    }
  }

  componentWillMount() {
    wp < hp
      ? this.setState({
          layout: 'block'
        })
      : this.setState({
          layout: 'inline'
        })
    console.log('login')
    let token = localStorage.getItem('token')
    if (token && token.length !== 0) {
      console.log(token)
      console.log('to home')
      this.setState({
        redirectToReferrer: true
      })
    }
  }

  onKeyPress = e => {
    // press enter key
    console.log(e.which)
    if (e.which === 13) {
      this.onSubmit(e)
    }
  }

  onSubmit = async e => {
    this.setState({ progress: true })
    setTimeout(this.onProgress, 1000)
  }

  onProgress = async () => {
    const { email, password } = this.state

    // const pass = Regex.passwordRegex.test(String(password).toLowerCase())
    const mail = Regex.emailRegex.test(String(email).toLowerCase())

    if (email === '') {
      this.setState({
        notitext: 'Please input your email',
        progress: false,
        isError: true
      })
      this.warning()
    } else {
      if (mail) {
        if (password === '') {
          this.setState({
            notitext: 'Please input your password',
            progress: false,
            isError: true
          })
          this.warning()
        } else {
          this.fetchApi()
        }
      } else {
        this.setState({
          notitext: 'Email syntax error',
          progress: false,
          isError: true
        })
        this.warning()
      }
    }
  }

  fetchApi = async () => {
    const { email, password } = this.state

    const payload = {
      email: email,
      password: password,
      device_id: 'onSite',
      device_name: 'onSite'
    }
    const url = Params.API_URL + '/account/login'
    const data = await fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(payload)
    })
      .then(response => response.json())
      .catch(error => error)
    if (data.status_code == 1000) {
      localStorage.setItem('token', data.data.token)
      localStorage.setItem('types', data.data.types)
      this.setState({ redirectToReferrer: true, progress: false })
    } else {
      this.setState({
        passError: true,
        progress: false,
        isError: true,
        notitext: data.message
      })
      this.warning()
    }
  }

  warning = title => {
    Modal.error({
      title: title,
      content: (
        <i style={{ color: 'red', textAlign: 'center' }}>
          {this.state.notitext}
        </i>
      )
    })
    this.setState({
      notitext: ''
    })
  }

  render() {
    const {
      password,
      email,
      redirectToReferrer,
      isError,
      progress,
      notitext
    } = this.state

    let { from } = this.props.location.state || {
      from: { pathname: '/admin/challenges' }
    }

    if (redirectToReferrer) return <Redirect to={from} />

    const component = (
      <React.Fragment>
        <Form layout='vertical'>
          <Form.Item>
            <Input
              prefix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />}
              value={email}
              placeholder='Username'
              autoFocus={true}
              onKeyPress={this.onKeyPress}
              onChange={e =>
                this.setState({
                  email: e.currentTarget.value
                })
              }
              style={{
                width: wp < hp ? wp * 0.8 + 'px' : wp * 0.21 + 'px',
                border: 0
              }}
            />
          </Form.Item>
          <Form.Item style={{ marginRight: 0 }}>
            <Input
              className='loginInput'
              prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />}
              value={password}
              type='password'
              placeholder='Password'
              onKeyPress={this.onKeyPress}
              onChange={e =>
                this.setState({
                  password: e.currentTarget.value
                })
              }
              style={{
                border: 0,
                width: wp < hp ? wp * 0.8 + 'px' : wp * 0.21 + 'px'
              }}
            />
          </Form.Item>
        </Form>
        <Form>
          <Button
            type='primary'
            htmlType='submit'
            onClick={this.onSubmit}
            style={{
              borderRadius: '4px',
              marginTop: '3px',
              width: wp < hp ? wp * 0.8 + 'px' : wp * 0.21 + 'px',
              fontFamily: 'Lato',
              fontWeight: 'bold',
              fontSize: '16px'
            }}
          >
            Log in
          </Button>
        </Form>
        <div
          style={{
            width: wp < hp ? wp * 0.8 + 'px' : wp * 0.21 + 'px',
            marginTop: '5px'
          }}
        />
      </React.Fragment>
    )

    return (
      <LoginStyle
        style={{ height: '100vh' }}
        className='site_wrapper d-flex flex-column align-items-center justify-content-center'
        id='site_wrapper'
      >
        <img
          src={Backgrounds.Logo}
          style={{
            marginTop: -hp * 0.065 + 'px',
            marginBottom: hp * 0.065 + 'px',
            width: wp < hp ? wp * 0.205 + 'px' : wp * 0.053 + 'px'
          }}
        />
        <FromLoginStyle>
          <div
            style={{
              width: wp < hp ? wp * 0.8 + 'px' : wp * 0.21 + 'px',
              textAlign: 'center'
            }}
          >
            <TitleStyle>LOG IN</TitleStyle>
          </div>

          <Spin spinning={progress} delay={500}>
            {component}
          </Spin>
        </FromLoginStyle>
      </LoginStyle>
    )
  }
}

export default Login

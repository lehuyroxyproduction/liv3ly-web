import styled from "styled-components";
import { Backgrounds ,wp,hp ,Colors} from '../../contants/contants'

export const LoginStyle = styled.div`
    background-image: url(${Backgrounds.BgLogin});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    .ant-input{
        border: 0 !important;
    }
    .btnLogin:{
        width: ${wp < hp ? wp*0.8+"px" : wp * 0.21 +"px"},
        border-radius:"4px",
        marginTop:"3px",
        font-family: 'Lato', sans-serif;
        font-weight:'bold';
        font-size:16px
    }
`

export const FromLoginStyle =styled.div`
    background-color: ${Colors.white};
    padding:30px 20px ;
    border-radius: 8px;
`
export const TitleStyle=styled.h1`
font-family: 'Lato', sans-serif;
font-weight:'bold';
font-size:28px
`